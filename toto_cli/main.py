"""
Exo-Import is a program to help in importation in the exoplanets.eu catalog.

Its can be used for all exo-objects (exoplanets, exomoons, discs, comets...).

It is a TYPER program that can launch the different verbs (actions) with their
respective options.
"""

# First party imports
from .cli import APP

def main():
    """Launch cli application for Exo Import."""
    APP()

if __name__ == "__main__":
    main()
