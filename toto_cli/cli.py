"""App for typer to interact with a user in command lines."""

# Third party imports
import typer
from rich.console import Console

from test_cli_executable import toto

# App manager
APP = typer.Typer(
    help="[bold blue on black]toto.[/bold blue on black]",
    rich_markup_mode="rich",
)

# Rich console
CONSOLE = Console()

def version_callback(value: bool) -> None:
    if value:
        CONSOLE.print(f"Toto v.1.0")
        raise typer.Exit()


@APP.command(
    name="toto",
    help="[bold white on black]Display Toto.[/bold white on black]",
)
def cli_toto():
    CONSOLE.print(toto())

@APP.callback()
def main(
    _version: bool = typer.Option(
        False,
        "--version",
        "-v",
        help="Show the application's version and exit.",
        callback=version_callback,
        is_eager=True,
    ),
) -> None:
    return
